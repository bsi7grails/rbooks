package br.com.rbooks

class ItemComprado {

    int quantidade
    double valorVenda

    // ASSOCIAÇÃO ENTRE AS CLASSES COMPRA E PRODUTO:
    Compra compra
    Produto produto

    // QUEM MANDA NA ASSOCIAÇÃO:
    static belongsTo = [Compra]

    static constraints = {
    }
}