package br.com.rbooks

class Compra {

    Date dataCompra
    double valorTotal

    // RELACIONAMENTO COM A CLASSE USU�RIO:
    Usuario usuario

    // RELACIONAMENTO COM A CLASSE ITEMCOMPRADO:
    static hasMany = [itens:ItemComprado]

    static constraints = {
    }
}