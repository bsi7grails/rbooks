package br.com.rbooks

class Livro extends Produto{

    String nomeAutor
    String generoLivro
    String editora
    String prologo

    static constraints = {
    }
}