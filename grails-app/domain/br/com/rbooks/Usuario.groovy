package br.com.rbooks

class Usuario {

    String nomeUsuario
    String cpf
    String email
    String telefone
    Date dataNascimento

    // RELACIONAMENTO COM A CLASSE COMPRA:
    static hasMany = [compras:Compra]

    static constraints = {
    }
}