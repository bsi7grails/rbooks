package br.com.rbooks

class Produto {

    String nomeProduto
    Integer edicao
    double preco

    // RELACIONAMENTO COM A CLASSE ESTOQUE:
    Estoque estoque

    // RELACIONAMENTO COM A CLASSE ITEMCOMPRADO:
    static hasMany = [itens:ItemComprado]

    static constraints = {
    }
}