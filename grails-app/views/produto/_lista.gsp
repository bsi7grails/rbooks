<g:if test="${produtos.size() > 0}">
    <table>
        <tr>
            <th> Nome </th>
            <th> Edição </th>
            <th> Preço </th>
            <th> Quantidade </th>
            <th> Mínimo </th>
            <th> Opções </th>
        </tr>

    <!-- FOR SIMPLIFICADO -->
        <g:each var="produto" in="${produtos}">
            <tr>
                <td> ${produto.nomeProduto} </td>
                <td> ${produto.edicao} </td>
                <td> ${produto.preco} </td>
                <td> ${produto.estoque?.quantidade} </td>
                <td> ${produto.estoque?.quantidadeMinima} </td>
                <td>
                    <g:remoteLink controller="produto" action="alterar" update="formularioCadastro" id="${produto.id}"> Alterar Produto </g:remoteLink> -
                    <a href="#" onclick="excluir('${produto.id}')"> Excluir Item </a>
                </td>
            </tr>
        </g:each>
    </table>
</g:if>
<g:else>
    Não há itens cadastrados no sistema!
</g:else>