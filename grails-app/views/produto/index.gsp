<%--
  Created by IntelliJ IDEA.
  User: Rany Moura
  Date: 04/06/2015
  Time: 02:57
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main" />
    <title> Produtos - Banca BSITAP </title>
    <g:javascript library="jquery" />

    <script type="text/javascript">
<!-- CARREGANDO A LISTA DE PRODUTOS DINAMICAMENTE: -->
    function carregarLista(){
        <g:remoteFunction controller="produto" action="listar" update="listaDeProduto"></g:remoteFunction>
    }

    function excluir(id){
        if(confirm("Deseja excluir este item da lista de produtos?")){
            <g:remoteFunction controller="produto" action="excluir" update="listaDeProduto" id="'+id+'"></g:remoteFunction>
        }
    }

<!-- TRABALHO DE GENTE PREGUIÇOSA... MAS TA VALENDO, PELA FALTA DE TEMPO!! -->
    function cancelar(){
        $("#formularioCadastro").html("")
    }

    </script>
</head>

<body>
<!-- ESTE LINK SERÁ CARREGARÁ O FORMULÁRIO DE CADASTRO DA DIV: "formularioCadastro"-->
    <g:remoteLink controller="produto" action="adicionar" update="formularioCadastro"> Adicionar Produto </g:remoteLink>
<!-- IRÁ SER EXIBIDO O TEMPLATE _lista -->
    <div id="listaDeProduto">
        <g:render template="lista" model="[produtos: produtos]"> </g:render>
    </div>

    <div id="formularioCadastro">

    </div>
</body>
</html>