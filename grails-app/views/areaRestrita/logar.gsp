<%--
  Created by IntelliJ IDEA.
  User: Rany Moura
  Date: 14/06/2015
  Time: 23:21
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title> Área Restrita - Banca BSITAP </title>
</head>

<!-- VERIFICANDO SE O USUÁRIO EXISTE COM O J_SPRING_SECURITY_CHECK-->
    <form action='${request.contextPath}/j_spring_security_check' method='post' id='formularioLogar' name='formularioLogar'>
        <fieldset> Informe seus dados
            <p>
                <label> E-mail: <input type="text" name="j_username" id="username" /> </label>
            </p>

            <p>
                <label> Senha: <input type="password" name="j_password" id="password" /> </label>
            </p>

            <p>
                <label> <input type="submit" value="Entrar" /> </label>
            </p>
        </fieldset>
    </form>
<!-- PARA QUE SEJA USADO O J_SPRING_SECURITY_CHECK, O NAME DEVE SEGUIR O PADRÃO DESCRITO ACIMA -->

<body>

</body>
</html>