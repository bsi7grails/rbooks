
package br.com.rbooks

class ProdutoController {

    def index() {

        def lista = Produto.list()
        render(view:"/produto/index", model: [produtos: lista])
    }

    //MOSTRAR OS CAMPOS PARA ADICIONAR UM PRODUTO:
    def adicionar(){

        Produto novoProduto = new Produto()
        novoProduto.edicao = 0
        novoProduto.preco = 0
        novoProduto.estoque = new Estoque()
        novoProduto.estoque.quantidade = 0
        novoProduto.estoque.quantidadeMinima = 0

        render(template: "/produto/formulario", model: [produto: novoProduto])

    }

    //OP��O SALVAR:
    def salvar(){

        Produto novoProduto = new Produto()

        novoProduto.nomeProduto = params.nomeProduto
        novoProduto.edicao = params.edicao.toInteger()
        novoProduto.preco = params.preco.toDouble()
        novoProduto.estoque = new Estoque()
        novoProduto.estoque.quantidade = params.quantidade.toInteger()
        novoProduto.estoque.quantidadeMinima = params.quantidadeMinima.toInteger()

        //PEGANDO ERROS DA CLASSES DE DOM�NIO E SALVANDO:
        novoProduto.validate()
        if (!novoProduto.hasErrors()){
            novoProduto.save(flush: true)
            render("Produto cadastrado com sucesso!")
        }else {
            render("Ocorreu um erro ao tentar cadastrar o produto!")
        }

    }

    //ATUALIZANDO TEMPLATE NA TELA:
    def listar(){

        def lista = Produto.list()
        render(template:"/produto/lista", model: [produtos: lista])

    }

    //EXIBINDO UM PRODUTO PARA SER ALTERADO:
    def alterar(){

        Produto produto = Produto.get(params.id)
        render(template:"/produto/formulario", model: [produto: produto])
    }

    def excluir(){

        Produto produto = Produto.get(params.id)
        produto.delete(flush: true)

        def lista = Produto.list()
        render(template:"/produto/lista", model: [produtos: lista])

    }
}