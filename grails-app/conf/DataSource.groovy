dataSource {
    pooled = true
    jmxExport = true
    driverClassName = "org.postgresql.Driver"
    username = "postgres"
    password = "BSIpostgres"
}

hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = true
    //    cache.region.factory_class = 'org.hibernate.cache.SingletonEhCacheRegionFactory' // Hibernate 3
    cache.region.factory_class = 'org.hibernate.cache.ehcache.EhCacheRegionFactory' // Hibernate 4
}

// environment specific settings
environments {

    development {
        dataSource {
            dbCreate = "update" // one of 'create', 'create-drop', 'update', 'validate', ''
            url = "jdbc:postgresql://localhost:5432/BSI-TAP-2015"
        }
    }

    test {
        dataSource {
            dbCreate = "update"
            url = "jdbc:postgresql://localhost:5432/BSI-TAP-2015"
        }
    }

    production {
        dataSource {
            dbCreate = "update"
            url = "jdbc:postgresql://localhost:5432/BSI-TAP-2015"
        }
    }
}